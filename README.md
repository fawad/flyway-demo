 # Prerequisites

 * [Flyway](https://flywaydb.org/download/community)
 * [Java](https://www.azul.com/downloads/zulu/)
 * A SQL Database (SQL Server, PostgreSQL etc)