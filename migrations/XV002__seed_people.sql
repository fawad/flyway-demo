CREATE TABLE flywaydemo.people (nickname NVARCHAR(255) PRIMARY KEY,
    location NVARCHAR(255)
    );

INSERT INTO flywaydemo.people (nickname, location) VALUES ('thor', 'Asgård'),
    ('ironman', 'Midgård');